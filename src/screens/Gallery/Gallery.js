/**
 * Created by mata on 11/29/18.
 */


import React, { Component } from "react";
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    Button,
    FlatList,
    Alert,
    ImageBackground
} from "react-native";
import { connect } from "react-redux";
import { Navigation } from "react-native-navigation";

import backgroundImage from "../../assets/ikantara_homescreen.jpg";

var {height, width} = Dimensions.get('window');

class Gallery extends Component {
    static navigatorStyle = {
        navBarTextColor: 'white',
        navBarBackgroundColor: '#043673',
        navBarButtonColor: 'white',
        // topBarElevationShadowEnabled: false,
        navBarHidden: true
    };

    OnClick = () => {
        console.log('start play')
        this.props.navigator.push({
            screen: "ikantara.FinishInfo",
        });
    };




    render() {
        var {height, width} = Dimensions.get('window');
        return (

            <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
                <View>
                    <Text>Gallery</Text>
                </View>
                <View style={{flex:1, flexDirection: "row", justifyContent:'center', alignItems:'center'}}>
                    <TouchableOpacity onPress={this.OnClick}>
                        <View style={styles.viewFish}>
                            <Image  style={{width: height/3,height:height/3,resizeMode:'contain'}}
                                    source={require('../../assets/gambar_penyuw.jpg')}
                            />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.OnStartButton2}>
                        <View style={styles.viewFish}>
                            <Image style={styles.imgFish}
                                   source={require('../../assets/ikan2.png')}
                            />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.OnStartButton2}>
                        <View style={styles.viewFish}>
                            <Image style={styles.imgFish}
                                   source={require('../../assets/ikan3.png')}
                            />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.OnStartButton2}>
                        <View style={styles.viewFish}>
                            <Image style={styles.imgFish}
                                   source={require('../../assets/ikan4.png')}
                            />
                        </View>
                    </TouchableOpacity>
                </View>

                <View style={{flex:1, flexDirection: "row", justifyContent:'center', alignItems:'center'}}>
                    <TouchableOpacity onPress={this.OnStartButton2}>
                        <View style={styles.viewFish}>
                            <Image style={styles.imgFish}
                                   source={require('../../assets/ikan5.png')}
                            />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.OnStartButton2}>
                        <View style={styles.viewFish}>
                            <Image style={styles.imgFish}
                                   source={require('../../assets/ikan6.png')}
                            />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.OnStartButton2}>
                        <View style={styles.viewFish}>
                            <Image style={styles.imgFish}
                                   source={require('../../assets/ikan7.png')}
                            />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.OnStartButton2}>
                        <View style={styles.viewFish}>
                            <Image style={styles.imgFish}
                                   source={require('../../assets/ikan8.png')}
                            />
                        </View>
                    </TouchableOpacity>
                </View>


            </ImageBackground>

        )
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        width: "100%",
        flex: 1,

    },
    imgFish: {
        width: height/3,
        resizeMode:'contain'
    },
    viewFish: {
        padding: 10,
    }
})

export default connect(null, null)(Gallery);