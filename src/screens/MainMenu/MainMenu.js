/**
 * Created by mata on 11/29/18.
 */


import React, { Component } from "react";
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    Button,
    FlatList,
    ImageBackground
} from "react-native";
import { connect } from "react-redux";
import { Navigation } from "react-native-navigation";

import backgroundImage from "../../assets/ikantara_homescreen.jpg";

class MainMenu extends Component {
    static navigatorStyle = {
        navBarTextColor: 'white',
        navBarBackgroundColor: '#043673',
        navBarButtonColor: 'white',
        // topBarElevationShadowEnabled: false,
        navBarHidden: true
    };

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    }

    OnStartButton = () => {
        console.log('start play')
        this.props.navigator.push({
            screen: "ikantara.LevelPage",
        });
    };

    OnGalleryButton = () => {
        console.log('start play')
        this.props.navigator.push({
            screen: "ikantara.Gallery",
        });
    };


    render() {
        var {height, width} = Dimensions.get('window');
        return (
            <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
                <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                    <Image
                        style={{ width:width/2,resizeMode:'contain'}}
                        source={require('../../assets/ikantara_texthome.png')}
                    />
                </View>
                <View style={{flex:1, flexDirection: "row", justifyContent:'center', alignItems:'center'}}>
                    <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                        <TouchableOpacity onPress={this.OnStartButton}>
                            <View>
                                <Image
                                    style={{width:width/3.5,resizeMode:'contain'}}
                                    source={require('../../assets/play_home_button.png')}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                        <TouchableOpacity onPress={this.OnGalleryButton}>
                            <View>
                                <Image
                                    style={{width:width/3.5,resizeMode:'contain'}}
                                    source={require('../../assets/gallery_text.png')}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>


            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        width: "100%",
        flex: 1,
    },
    imgbtn: {
        alignItems: "stretch",
        width: 10,
    }
})

export default connect(null, null)(MainMenu);