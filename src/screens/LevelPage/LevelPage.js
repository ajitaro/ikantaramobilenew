/**
 * Created by mata on 12/12/18.
 */

/**
 * Created by mata on 11/29/18.
 */


import React, {Component} from "react";
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    Button,
    FlatList,
    Alert,
    ImageBackground
} from "react-native";
import {connect} from "react-redux";
import backgroundImage from "../../assets/level_bg.png";
import {Navigation} from "react-native-navigation";
var {height, width} = Dimensions.get('window');
class LevelPage extends Component {
    static navigatorStyle = {
        navBarTextColor: 'white',
        navBarBackgroundColor: '#043673',
        navBarButtonColor: 'white',
        // topBarElevationShadowEnabled: false,
        navBarHidden: true
    };

    state = {
        img_indonesia: '1',
        wilayah:'Perairan Sumatra',
        isLocked: true
    }


    OnSelect = () => {
        if (!this.state.isLocked) {
            Alert.alert('INFO', 'Anda Tidak bisa ke level ini sebelum menyelesaikan Wilayah 1')
        } else {
            this.props.navigator.push({
                screen: "ikantara.ChooseFish",
            });
        }

    };

    OnStartButton = () => {
        console.log('1')
        this.setState({img_indonesia: '1', isLocked: true, wilayah:'Perairan Sumatra'})
    };

    OnStartButton2 = () => {
        console.log('2')
        this.setState({img_indonesia: '2', isLocked: false, wilayah:'Perairan Jawa'})
        // this.props.navigator.push({
        //     screen: "ikantara.ChooseFish",
        // });
    };

    OnStartButton3 = () => {
        console.log('3')
        this.setState({img_indonesia: '3', isLocked: false, wilayah:'Perairan Kalimantan'})
        // this.props.navigator.push({
        //     screen: "ikantara.ChooseFish",
        // });
    };

    OnStartButton4 = () => {
        console.log('4')
        this.setState({img_indonesia: '4', isLocked: false, wilayah:'Perairan Sulawesi'})
        // this.props.navigator.push({
        //     screen: "ikantara.ChooseFish",
        // });
    };

    OnStartButton5 = () => {
        console.log('5')
        this.setState({img_indonesia: '5', isLocked: false, wilayah:'Perairan Maluku & Papua'})
        // this.props.navigator.push({
        //     screen: "ikantara.ChooseFish",
        // });
    };

    render() {
        var {height, width} = Dimensions.get('window');
        let petaindonesia = null

        if (this.state.img_indonesia == '4') {
            petaindonesia = (
                <View>
                    <Image style={{height: height / 2.5, width:width*2/3,resizeMode: 'contain'}}
                           source={require('../../assets/sulawesi_dan_sundakecil_hijau.png')}
                    />
                </View>
            )
        } else if (this.state.img_indonesia == '5') {
            petaindonesia = (
                <View>
                    <Image style={{height: height / 2.5, width:width*2/3, resizeMode: 'contain'}}
                           source={require('../../assets/maluku_dan_papua_hiijau.png')}
                    />
                </View>
            )
        } else if (this.state.img_indonesia == '1') {
            petaindonesia = (
                <View>
                    <Image style={{height: height / 2.5, width:width*2/3, resizeMode: 'contain'}}
                           source={require('../../assets/sumatera_hijau.png')}
                    />
                </View>
            )
        } else if (this.state.img_indonesia == '2') {
            petaindonesia = (
                <View>
                    <Image style={{height: height / 2.5, width:width*2/3, resizeMode: 'contain'}}
                           source={require('../../assets/jawa_hijau.png')}
                    />
                </View>
            )
        } else if (this.state.img_indonesia == '3') {
            petaindonesia = (
                <View>
                    <Image style={{height: height / 2.5, width:width*2/3, resizeMode: 'contain'}}
                           source={require('../../assets/kalimantan_hijau.png')}
                    />
                </View>
            )
        }
        return (
            <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
                <View style={{flex: 1,justifyContent: 'center', alignItems: 'center'}}>
                    <View style={styles.lvlbtn}>
                        <TouchableOpacity onPress={this.OnStartButton}>

                                <ImageBackground style={styles.imglvlbtn}
                                                 source={require('../../assets/lvl_btn.png')}>
                                    <View style={styles.lvlword}>
                                        <Text style={styles.textlvl}>Wilayah 1</Text>
                                    </View>

                                </ImageBackground>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.lvlbtn}>
                        <ImageBackground style={styles.imglvlbtn}
                                         source={require('../../assets/lockedlvl_btn.png')}
                        >
                            <TouchableOpacity onPress={this.OnStartButton2}>
                                <View style={{padding: 5, justifyContent: 'flex-end', alignItems: 'center'}}>
                                    <Image style={{height: height / 12, resizeMode: 'contain'}}
                                           source={require('../../assets/gembok_png.png')}
                                    />
                                </View>
                            </TouchableOpacity>
                        </ImageBackground>
                    </View>

                    <View style={styles.lvlbtn}>
                        <ImageBackground style={styles.imglvlbtn}
                                         source={require('../../assets/lockedlvl_btn.png')}
                        >
                            <TouchableOpacity onPress={this.OnStartButton3}>
                                <View style={{padding: 5, justifyContent: 'flex-end', alignItems: 'center'}}>
                                    <Image style={{height: height / 12, resizeMode: 'contain'}}
                                           source={require('../../assets/gembok_png.png')}
                                    />
                                </View>
                            </TouchableOpacity>
                        </ImageBackground>
                    </View>

                    <View style={styles.lvlbtn}>
                        <ImageBackground style={styles.imglvlbtn}
                                         source={require('../../assets/lockedlvl_btn.png')}
                        >
                            <TouchableOpacity onPress={this.OnStartButton4}>
                                <View style={{padding: 5, justifyContent: 'flex-end', alignItems: 'center'}}>
                                    <Image style={{height: height / 12, resizeMode: 'contain'}}
                                           source={require('../../assets/gembok_png.png')}
                                    />
                                </View>
                            </TouchableOpacity>
                        </ImageBackground>
                    </View>
                    <View style={styles.lvlbtn}>
                        <ImageBackground style={styles.imglvlbtn}
                                         source={require('../../assets/lockedlvl_btn.png')}
                        >
                            <TouchableOpacity onPress={this.OnStartButton5}>
                                <View style={{padding: 5, justifyContent: 'flex-end', alignItems: 'center'}}>
                                    <Image style={{height: height / 12, resizeMode: 'contain'}}
                                           source={require('../../assets/gembok_png.png')}
                                    />
                                </View>
                            </TouchableOpacity>
                        </ImageBackground>
                    </View>

                </View>

                <View style={{flex: 2, justifyContent: 'center', alignItems: 'center'}}>
                    {petaindonesia}
                    <View style={{paddingTop:10}}>
                        <Text style={{color:'white', fontWeight:'bold', fontSize:20}}>{this.state.wilayah}</Text>
                    </View>
                    <TouchableOpacity onPress={this.OnSelect}>
                        <View style={{paddingTop: 20}}>
                            <Image style={{height: height / 7, resizeMode: 'contain'}}
                                   source={require('../../assets/select_btn.png')}
                            />
                        </View>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        width: "100%",
        flex: 1,
        flexDirection: "row"
    },
    imglvlbtn: {
        height: height / 8,
        width: width / 3.5,
    },
    lvlbtn: {
        padding: 10
    },
    gembokbtn: {
        height: height / 12,
        resizeMode: 'contain'
    },
    lvlword: {
        flex: 1,
        justifyContent: 'center', alignItems: 'center'
    },
    textlvl: {
        color: "#ffffff",
        fontSize: 20,
        fontWeight: "bold"
    }
})

export default connect(null, null)(LevelPage);