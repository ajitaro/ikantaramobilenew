/**
 * Created by mata on 12/12/18.
 */

/**
 * Created by mata on 11/29/18.
 */


import React, { Component } from "react";
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    Button,
    FlatList,
    ImageBackground,
    PanResponder, // we want to bring in the PanResponder system
    Animated // we wil be using animated value
} from "react-native";
import { connect } from "react-redux";
import { Navigation } from "react-native-navigation";

var {height, width} = Dimensions.get('window');

import backgroundImage from "../../assets/game_bg.png";

class PuzzleGame extends Component {
    static navigatorStyle = {
        navBarTextColor: 'white',
        navBarBackgroundColor: '#043673',
        navBarButtonColor: 'white',
        // topBarElevationShadowEnabled: false,
        navBarHidden: true
    };


    constructor(props) {
        super(props);

        this.state = {
            pan: new Animated.ValueXY(),
            scale: new Animated.Value(1.1),
        };
    }

    componentWillMount() {
        this._panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

            onPanResponderGrant: (e, gestureState) => {
                // Set the initial value to the current state
                this.state.pan.setOffset({x: this.state.pan.x._value, y: this.state.pan.y._value});
                this.state.pan.setValue({x: 0, y: 0});
                Animated.spring(
                    this.state.scale,
                    { toValue: 1.1, friction: 3 }
                ).start();
            },

            // When we drag/pan the object, set the delate to the states pan position
            onPanResponderMove: Animated.event([
                null, {dx: this.state.pan.x, dy: this.state.pan.y},
            ]),

            onPanResponderRelease: (e, {vx, vy}) => {
                // Flatten the offset to avoid erratic behavior
                this.state.pan.flattenOffset();
                Animated.spring(
                    this.state.scale,
                    { toValue: 1, friction: 3 }
                ).start();
            }
        });

    }

    OnFinish = () => {
        this.props.navigator.push({
            screen: "ikantara.FinishInfo",
        });
    };



    render() {
        var {height, width} = Dimensions.get('window');
        // Destructure the value of pan from the state
        let { pan, scale, pan2, scale2 } = this.state;

        // Calculate the x and y transform from the pan value
        let [translateX, translateY] = [pan.x, pan.y];
        let rotate = '0deg';

        // Calculate the transform property and set it as a value for our style which we add below to the Animated.View component
        let imageStyle = {height:(height/2)-10, width:(height/2)-10,position:'absolute', transform: [{translateX}, {translateY}, {rotate}]};
        let imageStyle2 = {position:'absolute', transform: [{translateX}, {translateY}, {rotate}, {scale}]};

        return (
            <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
                <View style={{paddingLeft:10, paddingTop:10}}>
                    <View style={{borderWidth:1, borderColor:'white', height:(height/2)-20, width:(height/2)-20}}>
                        <Image style={styles.imgFish}
                               source={require('../../assets/gambar_penyu2.png')}
                        />
                    </View>
                    <View style={{borderWidth:1, borderColor:'white', height:(height/2)-20, width:(height/2)-20}}>
                        <Image style={styles.imgFish}
                               source={require('../../assets/gambar_penyu1.png')}
                        />
                    </View>
                </View>
                <View style={{paddingRight:10, paddingTop:10}}>
                    <View style={{borderWidth:1, borderColor:'white', height:(height/2)-20, width:(height/2)-20}}>
                        <Image style={styles.imgFish}
                               source={require('../../assets/gambar_penyu4.png')}
                        />
                    </View>
                    <View style={{borderWidth:1, borderColor:'white', height:(height/2)-20, width:(height/2)-20}}>

                    </View>
                </View>
                <View>
                    <TouchableOpacity onPress={this.OnFinish}>
                        <View style={{paddingTop:height-100,justifyContent:'center', alignItems:'center'}}>
                            <Image
                                style={{width:width/3.5,resizeMode:'contain'}}
                                source={require('../../assets/finish_button.png')}
                            />
                        </View>
                    </TouchableOpacity>

                    <Animated.View style={imageStyle} {...this._panResponder.panHandlers}>
                        <Image source={require('../../assets/gambar_penyu3.png')} />
                    </Animated.View>
                </View>
            </ImageBackground>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    backgroundImage: {
        width: "100%",
        flex: 1,
        flexDirection: "row"
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    imgFish: {
        height:(height/2)-20,
        width:(height/2)-20,
        resizeMode:'contain'
    },
});

export default connect(null, null)(PuzzleGame);