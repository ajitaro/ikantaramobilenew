/**
 * Created by mata on 12/14/18.
 */

/**
 * Created by mata on 12/12/18.
 */

/**
 * Created by mata on 11/29/18.
 */


import React, { Component } from "react";
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    Button,
    FlatList,
    Alert,
    ImageBackground
} from "react-native";
import { connect } from "react-redux";
import backgroundImage from "../../assets/background_tahukam.jpg";
import PenyuVideo from "../../assets/penyuvideo.mp4"
import Video from 'react-native-video';

var {height, width} = Dimensions.get('window');
class FinishInfo extends Component {
    static navigatorStyle = {
        navBarTextColor: 'white',
        navBarBackgroundColor: '#043673',
        navBarButtonColor: 'white',
        // topBarElevationShadowEnabled: false,
        navBarHidden: true
    };

    state = {
        img_indonesia : '1',
        isLocked:true
    }


    OnSelect = () => {
        this.props.navigator.push({
            screen: "ikantara.ChooseFish",
        });

    };

    OnStartButton = () => {
        console.log('1')
        this.setState({img_indonesia:'1', isLocked:true})
    };


    render() {
        var {height, width} = Dimensions.get('window');

        return (
            <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
                <View style={{flex:1, paddingLeft:20, paddingTop:20}}>

                    <View style={{backgroundColor:'orange', paddingLeft:10, marginTop:90, height:'50%',justifyContent:'center', alignItems:'center'}}>
                        <Text style={{textAlign:'center',color:'white', fontSize:24}}><Text style={{color:'#46bf00'}}>Penyu Hijau </Text>dapat hidup lebih dari 80 tahun lamanya</Text>
                    </View>
                    <View style={{marginTop:45,position:'absolute'}}>
                        <Image  style={{height:height/4,resizeMode:'contain'}}
                                source={require('../../assets/tahukah_kamuu.png')}
                        />
                    </View>

                    <View style={{paddingLeft:10, position:'absolute',paddingTop:height - 110}}>
                        <TouchableOpacity onPress={this.OnSelect}>
                        <Image  style={{height:height/8,resizeMode:'contain'}}
                                source={require('../../assets/continue_button.png')}
                        />
                        </TouchableOpacity>
                    </View>

                </View>

                <View style={{flex:1.5, justifyContent:'center',alignItems:'center'}}>
                        <View style={{paddingTop:30}}>
                            <Image  style={{height:180,width:230,resizeMode:'contain'}}
                                    source={require('../../assets/gambar_penyuw.jpg')}
                            />
                            <Video source={{uri: "https://youtube.com/watch?v=-xiDuv5rLss"}}
                                   style={{width:100, height:100}} />
                        </View>
                </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        width: "100%",
        flex: 1,
        flexDirection: "row"
    },
    imglvlbtn: {
        height: height/8,
        width: width/3.5,
        alignSelf: 'stretch'
    },
    lvlbtn: {
        padding:10
    },
    gembokbtn:{
        height: height/12,
        resizeMode:'contain'
    },
    lvlword: {
        flex: 1,
        justifyContent:'center', alignItems:'center'},
    textlvl: {
        color: "#ffffff",
        fontSize: 20,
        fontWeight: "bold"
    }
})

export default connect(null, null)(FinishInfo);