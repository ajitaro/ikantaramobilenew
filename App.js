import { Navigation } from "react-native-navigation";
import { Provider } from "react-redux";
import React, { Component } from 'react';

import MainMenu from "./src/screens/MainMenu/MainMenu";
import LevelPage from "./src/screens/LevelPage/LevelPage";
import PuzzleGame from "./src/screens/PuzzleGame/PuzzleGame";
import ChooseFish from "./src/screens/ChooseFish/ChooseFish";
import FinishInfo from "./src/screens/FinishInfo/FinishInfo";
import Gallery from "./src/screens/Gallery/Gallery";

import configureStore from "./src/store/configureStore";
const store = configureStore();

// Register Screens
Navigation.registerComponent(
    "ikantara.MainMenu",
    () => MainMenu,
    store,
    Provider
);

Navigation.registerComponent(
    "ikantara.LevelPage",
    () => LevelPage,
    store,
    Provider
);

Navigation.registerComponent(
    "ikantara.ChooseFish",
    () => ChooseFish,
    store,
    Provider
);

Navigation.registerComponent(
    "ikantara.PuzzleGame",
    () => PuzzleGame,
    store,
    Provider
);

Navigation.registerComponent(
    "ikantara.FinishInfo",
    () => FinishInfo,
    store,
    Provider
);

Navigation.registerComponent(
    "ikantara.Gallery",
    () => Gallery,
    store,
    Provider
);

Navigation.startSingleScreenApp({
    screen: {
        screen: "ikantara.MainMenu",
        title: "IKANTARA"
    },

});